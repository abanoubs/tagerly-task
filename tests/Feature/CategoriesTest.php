<?php

namespace Tests\Feature;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoriesTest extends TestCase
{
    /**
     * Test category creation API.
     *
     * @return void
     */
    public function testCreate()
    {
        $response = $this->post('api/category',[
            'name' => 'Category name',
            'description' => 'Category Description',
        ]);

        $response->assertStatus(201);
    }

    /**
     * Test categories List API.
     *
     * @return void
     */
    public function testList()
    {
        $response = $this->get('api/category');
        $count = Category::count();
        $this->assertCount($count,$response['data']);
    }

    /**
     * Test categories update API.
     *
     * @return void
     */
    public function testShow()
    {
        $response = $this->get('api/category/'.Category::first()->id);
        $response->assertJsonStructure(['id','name','description','parent_id']);
    }

    /**
     * Test categories update API.
     *
     * @return void
     */
    public function testUpdate()
    {
        $id = Category::first()->id;
        $response = $this->patch('api/category/'.$id,[
            'name' => 'Category name',
            'description' => 'Category Description',
            'parent_id' => Category::where('id','!=',$id)->first()->id
        ]);

        $response->assertStatus(200);
    }


    /**
     * Test categories delete API.
     *
     * @return void
     */
    public function testDelete()
    {
        $response = $this->delete('api/category/'.Category::latest()->first()->id);
        $response->assertStatus(204);
    }


}

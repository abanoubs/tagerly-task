<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TablesExistenceTest extends TestCase
{

    public function testTablesExistence()
    {
        $tables = \Schema::hasTable('categories') && \Schema::hasTable('products');

        $this->assertTrue($tables);
    }
}

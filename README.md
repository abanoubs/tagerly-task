# e-Commerce task for Tagerly #
## Overview ##

In this program I have created REST API for a basic e-commerce portal from that contains products and categories with many to many relationship between the product and categories and self relation between the product and itself.

### Languages and Frameworks ###
* PHP 7 & MySql >= 5.7
* Laravel 8 Framework

## Design and architecture ##
1. I have used mysql database to store the products and categories information. So, I have created two models for two tables `products` and `categories` and also created a `pivot` table called `category_product` to serve many to many relationship between the category and the product. And for the category itself I have created self relation to set the parent category.

2. Also, I have created two controllers `ProductController` and `CategoryController` to take care of all the required logic for the REST APIs.

3. As well as, I have created separate request classes `ProductRequest` and `CategoryRequest` to validate the user inputs against the APIs.

4. Finally, I have created seeders to insert dummy data into the database as well as a basic 11 test cases to be just a proof of concept of Unit test.

Note: I did not return the full url for the image putting in my consideration that the images end-point will be set later, but of course we can use transformer
or even to override the data that returned form the database and add the images path to it. 
## ERD ##

![picture](https://abanoubs.com/Tagerly_ERD.png)

### Installation ###
* Clone the project to your machine
* Rename `.env.example` file to be `.env` and set your mysql database information
* Run `composer install` to get all vendor dependencies
* Run `php artisan migrate` to create the tables `categories` , `products` and `category_product`
* Run `php artisan db:seed` to insert the dummy data for  `products` and `categories`
* You may need to set permission `777` to `Storage` directory 
* Run `php artisan key:generate` to generate application key
* Run this command to run the tests `php artisan test tests/Feature` and it should pass 11 test case


### Usage ###
To start the project run the following command

`php artisan serve`

and then, you will be able to access the application through

http://127.0.0.1:8000/

You can use this collection in your Postman to demonstrate the APIs
https://www.getpostman.com/collections/e2b0f5ec4c91abe87285



<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the products.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $products = Product::latest()->paginate(20);
        return response()->json($products);
    }

    /**
     * Store a newly created product.
     *
     * @param ProductRequest $request
     * @return JsonResponse
     */
    public function store(ProductRequest $request): JsonResponse
    {
        $image = $request->file('image')->store('product_images');
        $inputs = $request->only(['name','description','price','categories'])+['image' => $image];
        $product = Product::create($inputs);
        $product->categories()->attach($request->categories);
        return response()->json($product,201);
    }

    /**
     * Display the specified product.
     *
     * @param Product $product
     * @return JsonResponse
     */
    public function show(Product $product): JsonResponse
    {
        return response()->json($product->load('categories'));
    }


    /**
     * Update the specified product.
     *
     * @param ProductRequest $request
     * @param Product $product
     * @return JsonResponse
     */
    public function update(ProductRequest $request, Product $product): JsonResponse
    {
        $inputs = $request->only(['name','description','price','categories']);
        if($request->hasFile('image'))
            Storage::delete($product->image);
            $inputs['image'] = $request->file('image')->store('product_images');
        $product->update($inputs);
        $product->categories()->sync($request->categories);
        return response()->json($product);
    }

    /**
     * Remove the specified product.
     *
     * @param Product $product
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Product $product): JsonResponse
    {
        return response()->json($product->delete(),204);
    }
}

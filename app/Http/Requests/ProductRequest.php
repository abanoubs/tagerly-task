<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'name' => 'string|nullable',
            'image' =>'nullable|image|mimes:jpg,jpeg,png,gif,bmp',
            'description'=>'nullable|string',
            'price'=>'nullable|regex:/^\d{1,8}+(\.\d{1,2})?$/',
            'categories' => 'nullable|array',
            'categories.*' => 'nullable|exists:categories,id',
        ];

        // Apply these rules only with new model api
        if($this->method() == 'POST')
        {
            $rules['name'] = 'required|string';
            $rules['image'] = 'required|image|mimes:jpg,jpeg,png,gif,bmp|max:2048';
            $rules['description'] = 'required|string';
            $rules['price'] = 'required|regex:/^\d{1,8}+(\.\d{1,2})?$/';
        }

        return $rules;
    }


    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(['errors' => $validator->errors()], 422));
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'parent_id' => 'integer|exists:categories,id|nullable',
            'name' => 'string|nullable',
            'description' => 'string|nullable',
        ];

        // Apply these rules only with update model api
        if(in_array($this->method(),['PATCH','PUT']))
        {
            $rules['parent_id'] = 'integer|exists:categories,id|nullable|not_in:'.$this->category->id;
            $rules['name'] = 'string|required';
        }

        return $rules;
    }


    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(['errors' => $validator->errors()], 422));
    }
}

<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {

        $parent_category_id = Category::count() > 1 ? Category::first()->id : null;
        return [
            'name' => $this->faker->sentence(2),
            'description' => $this->faker->paragraph,
            'parent_id' => $parent_category_id,
        ];
    }
}
